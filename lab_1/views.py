from django.shortcuts import render
from datetime import datetime, date

mhs_name = 'Muhammad Ridho Ananda'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 4, 28) 
npm = 1706028682
college = 'Universitas Indonesia'
hobby = 'Reading / Watching'

def index(request):
    response = {'name': mhs_name, 
                'age': calculate_age(birth_date.year),
                'npm': npm,
                'college' : college,
                'hobby' : hobby,
                }
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
