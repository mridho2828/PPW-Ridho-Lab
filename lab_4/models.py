from django.db import models

# Create your models here.
class Activity(models.Model):
    date = models.DateField()
    time = models.TimeField()
    name = models.CharField(max_length=100)
    place = models.CharField(max_length=100, blank=True)
    category = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return self.name
    