from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Activity
from .forms import Activity_Form

# Create your views here.
def home(request):
    return render(request, 'home.html')

def feedback(request):
    return render(request, 'feedback.html')

def activity_add(request):
    response = {}
    response['activity_form'] = Activity_Form
    return render(request, 'add_activity.html', response)

def activity_post(request):
    response = {}
    form = Activity_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['date'] = request.POST['date']
        response['time'] = request.POST['time']
        response['name'] = request.POST['name']
        response['place'] = request.POST['place']
        response['category'] = request.POST['category']
        activity = Activity(date=response['date'], time=response['time'], name=response['name'],
                            place=response['place'], category=response['category'])
        activity.save()
        return HttpResponseRedirect('/lab-4/list_activity')
    else:
        return HttpResponseRedirect('/lab-4/add_activity')

def activity_list(request):
    response = {}
    activity = Activity.objects.all()
    response['activity'] = activity
    return render(request, 'list_activity.html', response)

def activity_delete_all(request):
    Activity.objects.all().delete()
    return HttpResponseRedirect('/lab-4/list_activity')
