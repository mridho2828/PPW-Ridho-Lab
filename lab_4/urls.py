from django.urls import re_path
from . import views

urlpatterns = [
    re_path(r'^$', views.home),
    re_path(r'^feedback/$', views.feedback, name='feedback'),
    re_path(r'^add_activity/$', views.activity_add, name='add_activity'),
    re_path(r'^post_activity/$', views.activity_post, name='post_activity'),
    re_path(r'^list_activity/$', views.activity_list, name='list_activity'),
    re_path(r'^delete_all_activity/$', views.activity_delete_all, name='delete_all_activity')
]
