from django import forms
from .models import Activity

class Activity_Form(forms.ModelForm):
    class Meta:
        model = Activity
        fields = '__all__'
        widgets = {
            'date': forms.DateInput(attrs={'type': 'date'}),
            'time': forms.TimeInput(attrs={'type': 'time'}),
        }
